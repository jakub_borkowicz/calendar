
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/24/2015 12:52:42
-- Generated from EDMX file: C:\Users\Ja\Desktop\Calendar\Calendar\Models\CalendarEntity.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [calendarDatabase];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_CalendarEvent]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Events] DROP CONSTRAINT [FK_CalendarEvent];
GO
IF OBJECT_ID(N'[dbo].[FK_CalendarUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Calendars] DROP CONSTRAINT [FK_CalendarUser];
GO
IF OBJECT_ID(N'[dbo].[FK_CalendarSharedCalendar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SharedCalendars] DROP CONSTRAINT [FK_CalendarSharedCalendar];
GO
IF OBJECT_ID(N'[dbo].[FK_SharedCalendarUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SharedCalendars] DROP CONSTRAINT [FK_SharedCalendarUser];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Events]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Events];
GO
IF OBJECT_ID(N'[dbo].[Calendars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Calendars];
GO
IF OBJECT_ID(N'[dbo].[SharedCalendars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SharedCalendars];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Login] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [TimeZone] float  NOT NULL
);
GO

-- Creating table 'Events'
CREATE TABLE [dbo].[Events] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [AllDay] bit  NOT NULL,
    [StartTime] datetime  NOT NULL,
    [EndTime] datetime  NOT NULL,
    [CalendarId] int  NOT NULL
);
GO

-- Creating table 'Calendars'
CREATE TABLE [dbo].[Calendars] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Color] nvarchar(max)  NOT NULL,
    [OwnerId] int  NOT NULL
);
GO

-- Creating table 'SharedCalendars'
CREATE TABLE [dbo].[SharedCalendars] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CalendarId] int  NOT NULL,
    [UserId] int  NOT NULL,
    [AccessLevel] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [PK_Events]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Calendars'
ALTER TABLE [dbo].[Calendars]
ADD CONSTRAINT [PK_Calendars]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SharedCalendars'
ALTER TABLE [dbo].[SharedCalendars]
ADD CONSTRAINT [PK_SharedCalendars]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [CalendarId] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [FK_CalendarEvent]
    FOREIGN KEY ([CalendarId])
    REFERENCES [dbo].[Calendars]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CalendarEvent'
CREATE INDEX [IX_FK_CalendarEvent]
ON [dbo].[Events]
    ([CalendarId]);
GO

-- Creating foreign key on [OwnerId] in table 'Calendars'
ALTER TABLE [dbo].[Calendars]
ADD CONSTRAINT [FK_CalendarUser]
    FOREIGN KEY ([OwnerId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CalendarUser'
CREATE INDEX [IX_FK_CalendarUser]
ON [dbo].[Calendars]
    ([OwnerId]);
GO

-- Creating foreign key on [CalendarId] in table 'SharedCalendars'
ALTER TABLE [dbo].[SharedCalendars]
ADD CONSTRAINT [FK_CalendarSharedCalendar]
    FOREIGN KEY ([CalendarId])
    REFERENCES [dbo].[Calendars]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CalendarSharedCalendar'
CREATE INDEX [IX_FK_CalendarSharedCalendar]
ON [dbo].[SharedCalendars]
    ([CalendarId]);
GO

-- Creating foreign key on [UserId] in table 'SharedCalendars'
ALTER TABLE [dbo].[SharedCalendars]
ADD CONSTRAINT [FK_SharedCalendarUser]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SharedCalendarUser'
CREATE INDEX [IX_FK_SharedCalendarUser]
ON [dbo].[SharedCalendars]
    ([UserId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------