﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Calendar.Models
{
    [MetadataType(typeof(UserMetadata))]
    public partial class User
    {
        public User(string login, string password, double timeZone)
        {
            Login = login;
            Password = password;
            TimeZone = timeZone;
        }

    }
}