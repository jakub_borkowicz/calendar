﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Calendar.Models
{
    public partial class SharedCalendar
    {
        public SharedCalendar(int calendarId, int userId, AccessLevel accessLevel)
        {
            CalendarId = calendarId;
            UserId = userId;
            AccessLevel = accessLevel;
        }

        public SharedCalendar()
        {

        }

    }
}