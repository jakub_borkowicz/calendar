﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Calendar.Models
{
    [MetadataType(typeof(EventMetadata))]
    public partial class Event
    {
        public Event(string title, string description)
        {
            Title = title;
            Description = description;
        }

        public Event()
        {
            EndTime = DateTime.Now;
        }

        public Event(DateTime startTime)
        {
            StartTime = startTime;
            EndTime = startTime;
        }

        /// <summary>
        /// Convert time to UTC time.
        /// </summary>
        /// <param name="addTime"></param>
        public void ChangeTimesToUTC(double addTime)
        {
            if ((addTime % 1) == 0)
            {
                StartTime = StartTime.AddHours((int)-addTime);
                EndTime = EndTime.AddHours((int)-addTime);
            }
            else
            {
                StartTime = StartTime.AddHours((int)-addTime);
                EndTime = EndTime.AddHours((int)-addTime);

                StartTime = StartTime.AddMinutes(-30);
                EndTime = EndTime.AddMinutes(-30);
            }
        }


        /// <summary>
        /// Convert time to selected by user time zone
        /// </summary>
        /// <param name="addTime"></param>
        public void ChangeToLocalTime(double addTime)
        {
            if ((addTime % 1) == 0)
            {
                StartTime = StartTime.AddHours((int)addTime);
                EndTime = EndTime.AddHours((int)addTime);
            }
            else
            {
                StartTime = StartTime.AddHours((int)addTime);
                EndTime = EndTime.AddHours((int)addTime);

                StartTime = StartTime.AddMinutes(30);
                EndTime = EndTime.AddMinutes(30);
            }
        }
    }
}