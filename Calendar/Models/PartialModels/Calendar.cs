﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Calendar.Models
{
    [MetadataType(typeof(CalendarMetadata))]
    public partial class Calendar
    {
        public Calendar(string name, string color)
        {
            Name = name;
            Color = color;
        }

        /// <summary>
        /// Return events which starts at date passed in parameter.
        /// </summary>
        /// <param name="dateToCompare"></param>
        /// <returns></returns>
        public IEnumerable<Event> GetDateEvents(DateTime dateToCompare)
        {
            return Events.Where(e => e.StartTime.Day == dateToCompare.Day && e.StartTime.Month == dateToCompare.Month && e.StartTime.Year == dateToCompare.Year);
        }

        /// <summary>
        /// Return new date which day is always 1st but month and year are taken from parameter date.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetFirstDayDate(DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        /// <summary>
        /// Return access level of user with Id passed in parameter.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public AccessLevel GetUserAccessLevel(int userId)
        {
            if (OwnerId == userId)
            {
                return AccessLevel.Owner;
            }

            SharedCalendar sc = SharedCalendars.FirstOrDefault(s => s.CalendarId == Id && s.UserId == userId);
            if (sc == null)
            {
                return AccessLevel.None;
            }

            return sc.AccessLevel;
        }
    }
}