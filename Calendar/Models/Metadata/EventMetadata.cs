﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Calendar.Models
{
    public class EventMetadata
    {
        [Required]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        public bool AllDay { get; set; }

        [DataType(DataType.DateTime)]
        public System.DateTime StartTime { get; set; }

        [DataType(DataType.DateTime)]
        public System.DateTime EndTime { get; set; }
    }
}