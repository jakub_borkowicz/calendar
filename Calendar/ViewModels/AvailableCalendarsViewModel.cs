﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Calendar.Models;

namespace Calendar.ViewModels
{
    /// <summary>
    /// View Model used for holding data about owned calendars and shared calendars.
    /// </summary>
    public class AvailableCalendarsViewModel
    {
        public IEnumerable<Models.Calendar> MyCalendars { get; set; }
        public IEnumerable<Models.Calendar> SharedCalendars { get; set; }

        public AvailableCalendarsViewModel(IEnumerable<Models.Calendar> myCalendars, IEnumerable<Models.Calendar> sharedCalendars)
        {
            MyCalendars = myCalendars;
            SharedCalendars = sharedCalendars;
        }
    }
}