﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Calendar.Models;

namespace Calendar.ViewModels
{
    /// <summary>
    /// View model used in calendar page. 
    /// </summary>
    public class CalendarViewModel
    {
        public Calendar.Models.Calendar Calendar { get; set; }

        public DateTime SelectedDate { get; set; }

        public AccessLevel AccessLevel { get; set; }

        public IEnumerable<User> UsersToShare { get; set; }
        public IEnumerable<User> UsersShared { get; set; }

        public CalendarViewModel(Models.Calendar calendar, DateTime selectedDate, AccessLevel accessLevel)
        {
            Calendar = calendar;
            SelectedDate = selectedDate;
            AccessLevel = accessLevel;
        }
    }
}