﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Calendar.Models;

namespace Calendar.ViewModels
{
    /// <summary>
    /// View model used for registration of new users
    /// </summary>
    public class RegistrationUser
    {
        [Required]
        [DataType(DataType.Text)]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [Display(Name = "Password confirmation")]
        public string RepeatPassword { get; set; }

        [Required]
        public double TimeZone { get; set; }
    }
}