﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Calendar.Models;

namespace Calendar.ViewModels
{
    /// <summary>
    /// Calendar user in event details page.
    /// </summary>
    public class EventDetailsViewModel
    {
        public Event Event { get; set; }
        public AccessLevel AccessLevel { get; set; }

        public EventDetailsViewModel(Event @event, AccessLevel accessLevel)
        {
            Event = @event;
            AccessLevel = accessLevel;
        }
    }
}