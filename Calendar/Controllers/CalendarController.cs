﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Calendar.Models;
using Calendar.ViewModels;

namespace Calendar.Controllers
{
    public class CalendarController : Controller
    {

        /// <summary>
        /// Return view where user may create new calendar.
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            if (Session["LoggedUser"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            return View();
        }

        /// <summary>
        /// Adds new calendar to database. When validation fails sends back to Create page where user may fix errors.
        /// </summary>
        /// <param name="calendar"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(Models.Calendar calendar)
        {
            if (Session["LoggedUser"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            CalendarEntityContainer db = new CalendarEntityContainer();
            if (db.Calendars.Any(c => c.Name.Equals(calendar.Name, StringComparison.InvariantCultureIgnoreCase)))
            {
                ModelState.AddModelError("", "Calendar with this name already exists");
            }

            if (!ModelState.IsValid)
            {
                return View(calendar);
            }

            User user = (User)Session["LoggedUser"];

            calendar.OwnerId = user.Id;

            db.Calendars.Add(calendar);
            db.SaveChanges();
            return RedirectToAction("Show", new { id = calendar.Id, selectedDate = DateTime.Now });
        }

        /// <summary>
        /// Show selected calendar. Parameter SelectedDate is used to define on which month and year calendar should be shown.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="selectedDate"></param>
        /// <returns></returns>
        public ActionResult Show(int id, DateTime selectedDate)
        {
            if (Session["LoggedUser"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            CalendarEntityContainer db = new CalendarEntityContainer();

            Models.Calendar calendar = db.Calendars.FirstOrDefault(c => c.Id == id);

            if (calendar == null)
            {
                return new HttpNotFoundResult();
            }

            AccessLevel accessLevel = calendar.GetUserAccessLevel(((User)Session["LoggedUser"]).Id);

            if (accessLevel == AccessLevel.None)
            {
                return new HttpUnauthorizedResult();
            }

            foreach (Event item in calendar.Events)
            {
                item.ChangeToLocalTime(((User)Session["LoggedUser"]).TimeZone);
            }

            CalendarViewModel calendarViewModel = new CalendarViewModel(calendar,
                Models.Calendar.GetFirstDayDate(selectedDate), accessLevel);

            if (accessLevel == AccessLevel.Owner)
            {
                int userId = ((User)Session["LoggedUser"]).Id;
                IEnumerable<User> usersToShare = db.Users.Where(u => u.Id != userId && !db.SharedCalendars.Any(s => s.CalendarId == calendar.Id && s.UserId == u.Id));
                calendarViewModel.UsersToShare = usersToShare;

                IEnumerable<User> usersShared = db.Users.Where(u => u.Id != userId && db.SharedCalendars.Any(s => s.CalendarId == calendar.Id && s.UserId == u.Id));
                calendarViewModel.UsersShared = usersShared;
            }

            return View(calendarViewModel);
        }

        /// <summary>
        /// Gives selected access level to user. Redirects to Show page.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <param name="access"></param>
        /// <param name="selectedDate"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Share(int id, int userId, AccessLevel access, DateTime selectedDate)
        {
            if (Session["LoggedUser"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            CalendarEntityContainer db = new CalendarEntityContainer();
            User user = db.Users.FirstOrDefault(u => u.Id == userId);

            if (user == null)
            {
                return new HttpNotFoundResult();
            }

            Models.Calendar calendar = db.Calendars.FirstOrDefault(c => c.Id == id);

            if (calendar == null)
            {
                return new HttpNotFoundResult();
            }

            AccessLevel accessLevel = calendar.GetUserAccessLevel(((User)Session["LoggedUser"]).Id);

            if (accessLevel != AccessLevel.Owner)
            {
                return new HttpUnauthorizedResult();
            }

            db.SharedCalendars.Add(new SharedCalendar(id, userId, access));
            db.SaveChanges();
            TempData.Add("SharedCalendar", user.Login);
            return RedirectToAction("Show", new { id = id, selectedDate = selectedDate });
        }

        /// <summary>
        /// Remove access level to calendar from selected user. Redirects to Show page.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <param name="selectedDate"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RemoveAccess(int id, int userId, DateTime selectedDate)
        {
            if (Session["LoggedUser"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            CalendarEntityContainer db = new CalendarEntityContainer();
            User user = db.Users.FirstOrDefault(u => u.Id == userId);

            if (user == null)
            {
                return new HttpNotFoundResult();
            }

            Models.Calendar calendar = db.Calendars.FirstOrDefault(c => c.Id == id);

            if (calendar == null)
            {
                return new HttpNotFoundResult();
            }

            AccessLevel accessLevel = calendar.GetUserAccessLevel(((User)Session["LoggedUser"]).Id);

            if (accessLevel != AccessLevel.Owner)
            {
                return new HttpUnauthorizedResult();
            }

            SharedCalendar scToDelete =
                db.SharedCalendars.FirstOrDefault(s => s.CalendarId == id && s.UserId == userId);
            db.SharedCalendars.Remove(scToDelete);
            db.SaveChanges();
            TempData.Add("RemovedAccess", user.Login);
            return RedirectToAction("Show", new { id = id, selectedDate = selectedDate });
        }
    }
}
