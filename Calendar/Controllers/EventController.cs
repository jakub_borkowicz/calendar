﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Calendar.Models;
using Calendar.ViewModels;

namespace Calendar.Controllers
{
    public class EventController : Controller
    {
        /// <summary>
        /// Show details page of selected event.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            if (Session["LoggedUser"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            CalendarEntityContainer db = new CalendarEntityContainer();
            Event calendarEvent = db.Events.FirstOrDefault(e => e.Id == id);
            if (calendarEvent == null)
            {
                return new HttpNotFoundResult();
            }

            calendarEvent.ChangeToLocalTime(((User)Session["LoggedUser"]).TimeZone);

            return View(new EventDetailsViewModel(calendarEvent, calendarEvent.Calendar.GetUserAccessLevel(((User)Session["LoggedUser"]).Id)));
        }

        /// <summary>
        /// Show page where user may create new event.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="startDate"></param>
        /// <returns></returns>
        public ActionResult Create(int id, DateTime startDate)
        {
            if (Session["LoggedUser"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            Event calendarEvent = new Event(startDate) { CalendarId = id };

            return View(calendarEvent);
        }

        /// <summary>
        /// Adds new event to database. When validation fails redirects user to Create page to fix errors.
        /// </summary>
        /// <param name="calendarEvent"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(Event calendarEvent)
        {
            if (Session["LoggedUser"] == null)
            {
                return RedirectToAction("Login", "User");
            }


            if (calendarEvent.AllDay)
            {
                calendarEvent.EndTime = calendarEvent.StartTime.AddDays(1);
            }

            if (calendarEvent.StartTime > calendarEvent.EndTime)
            {
                ModelState.AddModelError("", "Event end time cannot be before event start time.");
            }

            if (ModelState.IsValid)
            {
                CalendarEntityContainer db = new CalendarEntityContainer();

                Models.Calendar calendar = db.Calendars.FirstOrDefault(c => c.Id == calendarEvent.CalendarId);

                if (calendar == null)
                {
                    return new HttpNotFoundResult();
                }

                AccessLevel accessLevel = calendar.GetUserAccessLevel(((User)Session["LoggedUser"]).Id);

                if (accessLevel == AccessLevel.None || accessLevel == AccessLevel.Read)
                {
                    return new HttpUnauthorizedResult();
                }

                TempData["CreatedEvent"] = calendarEvent.Title;
                calendarEvent.ChangeTimesToUTC(((User)Session["LoggedUser"]).TimeZone);
                db.Events.Add(calendarEvent);
                db.SaveChanges();
                return RedirectToAction("Show", "Calendar", new { id = calendarEvent.CalendarId, selectedDate = calendarEvent.StartTime });
            }
            return View(calendarEvent);
        }

        /// <summary>
        /// Deletes selected event from database. Redirects to calendar main page.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (Session["LoggedUser"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            CalendarEntityContainer db = new CalendarEntityContainer();

            Event eventToDelete = db.Events.FirstOrDefault(e => e.Id == id);

            if (eventToDelete == null)
            {
                return new HttpNotFoundResult();
            }

            AccessLevel accessLevel = eventToDelete.Calendar.GetUserAccessLevel(((User)Session["LoggedUser"]).Id);

            if (accessLevel == AccessLevel.None || accessLevel == AccessLevel.Read)
            {
                return new HttpUnauthorizedResult();
            }

            int calendarId = eventToDelete.CalendarId;
            DateTime eventTime = eventToDelete.StartTime;

            TempData["DeletedEvent"] = eventToDelete.Title;
            db.Events.Remove(eventToDelete);
            db.SaveChanges();

            return RedirectToAction("Show", "Calendar",
                new { id = calendarId, selectedDate = eventTime });
        }


        /// <summary>
        /// Shows page where user may edit calendar.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            CalendarEntityContainer db = new CalendarEntityContainer();

            Event calendarEvent = db.Events.FirstOrDefault(e => e.Id == id);

            if (calendarEvent == null)
            {
                return new HttpNotFoundResult();
            }

            AccessLevel accessLevel = calendarEvent.Calendar.GetUserAccessLevel(((User)Session["LoggedUser"]).Id);

            if (accessLevel == AccessLevel.None || accessLevel == AccessLevel.Read)
            {
                return new HttpUnauthorizedResult();
            }

            calendarEvent.ChangeToLocalTime(((User)Session["LoggedUser"]).TimeZone);

            return View(calendarEvent);
        }

        /// <summary>
        /// Updates selected event in database.
        /// </summary>
        /// <param name="calendarEvent"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(Event calendarEvent)
        {
            if (Session["LoggedUser"] == null)
            {
                return RedirectToAction("Login", "User");
            }


            if (calendarEvent.AllDay)
            {
                calendarEvent.EndTime = calendarEvent.StartTime;
            }

            if (calendarEvent.StartTime > calendarEvent.EndTime)
            {
                ModelState.AddModelError("", "Event end time cannot be before event start time.");
            }

            if (ModelState.IsValid)
            {
                CalendarEntityContainer db = new CalendarEntityContainer();

                Models.Calendar calendar = db.Calendars.FirstOrDefault(c => c.Id == calendarEvent.CalendarId);

                if (calendar == null)
                {
                    return new HttpNotFoundResult();
                }

                AccessLevel accessLevel = calendar.GetUserAccessLevel(((User)Session["LoggedUser"]).Id);

                if (accessLevel == AccessLevel.None || accessLevel == AccessLevel.Read)
                {
                    return new HttpUnauthorizedResult();
                }

                calendarEvent.ChangeTimesToUTC(((User)Session["LoggedUser"]).TimeZone);
                db.Entry(calendarEvent).State = EntityState.Modified;
                db.SaveChanges();
                TempData["ModifiedEvent"] = calendarEvent.Title;
                return RedirectToAction("Show", "Calendar", new { id = calendarEvent.CalendarId, selectedDate = calendarEvent.StartTime });
            }
            return View(calendarEvent);
        }
    }
}
