﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Calendar.Models;
using Calendar.ViewModels;

namespace Calendar.Controllers
{
    public class UserController : Controller
    {
        /// <summary>
        /// Shows login page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            if (Session["LoggedUser"] != null)
            {
                return RedirectToAction("MyCalendars");
            }
            return View();
        }

        /// <summary>
        /// Tries to log in selected user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(User user)
        {
            if (Session["LoggedUser"] != null)
            {
                return RedirectToAction("MyCalendars");
            }

            CalendarEntityContainer db = new CalendarEntityContainer();

            User loggedUser = db.Users.FirstOrDefault(u =>
                        u.Login.Equals(user.Login, StringComparison.InvariantCulture) &&
                        u.Password.Equals(user.Password, StringComparison.InvariantCulture));

            if (loggedUser == null)
            {
                ModelState.AddModelError("", "There is no such user or password is incorrect");
            }

            if (ModelState.IsValid)
            {
                Session.Add("LoggedUser", loggedUser);
                return RedirectToAction("MyCalendars");
            }

            return View(user);
        }

        /// <summary>
        /// Show register page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            if (Session["LoggedUser"] != null)
            {
                return RedirectToAction("MyCalendars");
            }

            return View();
        }

        /// <summary>
        /// Tries to register user in database.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Register(RegistrationUser model)
        {
            if (Session["LoggedUser"] != null)
            {
                return RedirectToAction("MyCalendars");
            }

            CalendarEntityContainer db = new CalendarEntityContainer();

            if (model.TimeZone > 99)
            {
                ModelState.AddModelError("", "You must choose your time zone");
            }

            if (db.Users.Count(m => m.Login == model.Login) > 0)
                ModelState.AddModelError("", "User with the same login name already exists");

            if (ModelState.IsValid)
            {
                TempData.Add("RegisteredUser", model.Login);

                db.Users.Add(new User(model.Login, model.Password, model.TimeZone));
                db.SaveChanges();
                return RedirectToAction("Login");
            }
            return View(model);
        }

        /// <summary>
        /// Show My Calendars page where user may choose one of his calendar.
        /// </summary>
        /// <returns></returns>
        public ActionResult MyCalendars()
        {
            if (Session["LoggedUser"] == null)
            {
                return RedirectToAction("Login");
            }
            User user = (User)Session["LoggedUser"];
            CalendarEntityContainer db = new CalendarEntityContainer();
            IEnumerable<Models.Calendar> calendars = db.Calendars.Where(c => c.OwnerId == user.Id);
            IEnumerable<Models.Calendar> sharedCalendars = db.Calendars.Where(c => db.SharedCalendars.Any(w => w.UserId == user.Id && c.Id == w.CalendarId));

            return View(new AvailableCalendarsViewModel(calendars, sharedCalendars));
        }

        /// <summary>
        /// Log user out from application
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            Session.Remove("LoggedUser");
            return RedirectToAction("Login");
        }
    }
}
