# Tests made on calendar application
Below will be described how calendar application was tested, each part in own section. Additionally there are descriptions why some tests were made(except obvious cases)

## Registration

* Registering on new data
* Registering without inserting some data - all data must be provided to register user.
* Registering on actually registered users - system should block duplicated user names.

## Logging in

* Log in on existing user
* Log in without providing any information - system cannot pass such users.
* Log in with invalid username/password - password must match existing username

## Logging out

* Log out
* Check if after logging out user lost access to other sites(only registration and login should work)

## Creating calendar

* Creating calendar with unique name
* Creating calendar with existing name - calendars name must be unique.
* Creating calendar without providing name - name must be provided.
* Creating caendar with different colors - calendar should be created beside selected color

## Selecting available calendars

* Select one of owned calendars
* Select one of calendar which was shared with user - check if access level is proper

## Show calendar main page

* Check some different times if calendar is displayed properly
* Check on users with different access levels - owner must have all rights,  read access level allows only displaying already created events, write access level allows aditionally creating new event
* Check if share options are available just for calendar owner
* Check if users without access to calendar cannot enter this page - they should get HTTP Unauthorized Error

## Adding new event

* Adding event with all necessary data provided.
* Adding event without some data - must be validated, system cannot pass it.
* Adding event with end date before start date - it must be validated, because start date always must be before or equal to end date
* Adding event with existing title - title is not unique so it can be duplicated.
* Adding all-day event and non all-day event - to check if both options works well

## Event details page

* Show details page of all-day event and non all-day event - to check if both options are rendered correctly
* Check if calendar owner or users with write access have access to editing and deleting options
* Check if user with read access level cannot edit or delete event
* Check if all data are displayed correctly
* Check same event on users with different time zones to see if local times are computed correctly

## Event edit page

* Check if users with proper access level may access this page(owner,write).
* Check if users with other access level cannot access this page(read,none).
* Check if user may change all data and submit form
* Check if all fields are properly validated
* Check if event data changed properly

## Event deleting

* Check if users with proper access level may delete event(owner,write).
* Check if users with other access level cannot delete event(read,none).
* Check if deleted event is not available anymore(cannot be seen on calendar page, cannot ented details or edit page)

## Sharing calendar

* Check if calendar owner have access to this option
* Check if users which are not calendars owners may share it
* Check if shared calendar is available for selected user
* Check if may share calendar multiple times with one user - should not be possible.
* Check if both access levels works

## Removing access from calendar

* Check if calendar owner have access to this option
* Check if users which are not calendars owners may remove access from it
* Check if user with removed access can access selected calendar - should not be possible.
* Check if may share calendar multiple times with one user - should not be possible.
