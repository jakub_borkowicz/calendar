# Calendar App
This application main purpose is to schedule events and share them between users. It is a web application written in ASP.NET MVC. SQL Server Express is required to run this application. Google Chrome is preferred web browser.

## Application Features
Features which were implemented for this application:

* Registration - to registrate user must provide his login name, password and his time zone. Login name must be unique so if it is already taken user will have to change it.

* Logging in - to access functionality of application user must log in on registered account from LogIn page. To log in user has to input his login name and password.

* Creating calendar - to create calendar user must provide calendar name and calendar color(it will be used when displaying calendar, should be other than black because calendar text will not be visible). Owner of created calendar is automatically set to user which is logged in(actually creates calendar).

* Creating event - to create event user has to open calendar and select option to add new event under choosen date. New event needs title and description, also user has to provide start-time and end-time of event or choose option of all-day event. Created event is available only in calendar which was actually choosen. User must be owner of calendar to create new event or must have write access level.

* Event details - user may access details of event from calendar view. To show event details user has to choose selected event. From this page user may delete or edit it(if has appropriate permissions).

* Deleting event - user may delete selected event from details page. To do this user must have write access level or has to be owner of selected calendar.

* Editing event - user may delete edit event by selecting appropriate option from details page. After that user will be moved to page where he can edit all data. To edit event user must have write access level or has to be owner of selected calendar.

* Sharing calendar - user may share calendar with other users from calendar page. To do this user must be owner of this calendar. Share option is only available if it was not shared to all users yet. User may select "read access level" which allows other users only to watch details of events in this calendars, or "write access level" which additionally allows other users to create, edit and delete events.

* Removing access to calendar - user may remove access to calendar for users which were added to share list before. This option is available from calendar page only for owner of calendar.

* Time zones - every user may have different selected time zone. When he create event time is converted to UTC time(based on selected time zone) and saved on server. When other user wants to see this event, the time is downloaded from the server and converted from UTC to selected by user time zone.

## Technical documentation
Application is written using MVC(Model View Controller) pattern.
### Model component - business logic
Model in MVC pattern stores data that it retrieved to the controller and displayed in the view. Also it is responsible for business logic of application.
Models in this application are designed in Entity Framework 6.0 using model first approach.

Business logic in this application is divided on 4 models:
- user - holds the data about user(Login,Password,TimeZone), it also has access to owned calendar and shared calendars, one user may have multiple calendars and one user may have access to multiple shared calendars
- calendar - holds the data about calendar(Name, Color, Owner, Events), one calendar may have multiple events, one calendar have just one owner
- event - holds the data about event(Title, Description, Calendar, StartTime, EndTime, AllDayEvent(define if event lasts all day)), Event is connected only to one Calendar
- sharedCalendar - holds the data about the calendars shared to users(Calendar, User, AccessLevel)

### View component
View component in MVC pattern is responsible for displaying the data

List of views in application:
- Calendar(Create, Show)
- Event(Create, Details, Edit)
- User(Register, Login, MyCalendars)

### Controller component
Controller component is responsible for sending commands to the model to update the model's state (e.g., editing a document). It can also send commands to its associated view to change the view's presentation of the model. It is a link between model and view.

Controllers in application:
- CalendarController
- UserController
- EventController


